package game;

import java.util.ArrayList;

/**
 * 	Diese Klasse stellt das Schlachtfeld eines Spielers dar. Dabei hat jeder
 *  Spieler ein Objekt von Battlefield, welches bei ihm gespeichert wird. 
 *  Das Schlachtfeld speichert dabei den 'Zustand' der einzelnen Felder,
 *  ausserdem laeuft die Logik des schiessens ueber das Schlachtfeld, es 
 *  aendert den Zustand der Felder und fuehrt auch den "Schuss" auf das 
 *  jeweilige Schiff aus.
 * 
 * @author Tobias Tomski
 *
 */
public class Battlefield {

	/**
	 * Das Spielfeld eines Spielers als Array
	 */
	private int[][] field;
	
	/**
	 * Die verschiedenen moeglichen Zustaende eines Feldes.
	 */
	public static final int WATER = 0;
	public static final int SHIP = 1;
	public static final int SCRAP = 2;
	public static final int PUFFER = 3;
	public static final int WATERHIT = 4;
	private ArrayList<Ship> shipsPlaced;
	
	/**
	 * Standard-Konstruktor
	 */
	public Battlefield(){
		field = new int[10][10];
		shipsPlaced = new ArrayList<Ship>();
	}
	
	/**
	 * Zusaetzlicher Konstruktor, falls das Spiel geladen wird
	 * @param field Altes Feld, wird an das lokale Battlefield-Feld uebergeben
	 * @param placed Plazierte Schiffe, aus der Speicherdatei ausgelesen.
	 */
	public Battlefield(int[][] field, ArrayList<Ship> placed){
		this.field = field;
		this.shipsPlaced = placed;
	}
	
	public int[][] getField(){
		return field;
	}
	
	public ArrayList<Ship> getShipsPlaced(){
		return shipsPlaced;
	}

	/**
	 * statische Methode, welche eine Liste der platzierbaren Schiffe zurueck
	 * gibt. Wird in GameWindow benoetigt.
	 * @return Liste der Schiffe
	 */
	public static ArrayList<Ship> getShips() {
		ArrayList<Ship> possibleShips = new ArrayList<Ship>();
		Ship bs = new Battleship();
		possibleShips.add(bs);
		Ship c1 = new Cruiser();
		possibleShips.add(c1);
		Ship c2 = new Cruiser();
		possibleShips.add(c2);
		Ship d1 = new Destroyer();
		possibleShips.add(d1);
		Ship d2 = new Destroyer();
		possibleShips.add(d2);
		Ship d3 = new Destroyer();
		possibleShips.add(d3);
		Ship sm1 = new Submarine();
		possibleShips.add(sm1);
		Ship sm2 = new Submarine();
		possibleShips.add(sm2);
		Ship sm3 = new Submarine();
		possibleShips.add(sm3);
		Ship sm4 = new Submarine();
		possibleShips.add(sm4);
		return possibleShips;
	}

	/**
	 * Diese Methode wird aufgerufen, um ein Schiff auf dem Schlachtfeld zu
	 * platzieren. Dabei wird zunaechst getestet, ob die PLatzierung zulaessig
	 * ist, und anschliessend, falls dies der Fall ist, der Wert der 
	 * entsprechenden Felder geaendert, sowie ein Puffer um das Schiff herum
	 * erzeugt. Dieser Puffer zaehlt wie ein Wasserfeld, jedoch koennen dort
	 * keine Schiffe platziert werden.
	 * 
	 * @param ship Das Schiff, sodass die Laenge bekannt ist
	 * @param coords Startkoordinaten des Schiffes
	 * @param orientation Ausrichtung des Schiffes
	 * @return Gibt true zurueck, wenn die Platzierung erfolgreich war,
	 * ansonsten false
	 */
	public boolean place(Ship ship, intPair coords, int orientation) {
		int length = ship.getLength();
		if (isValid(coords, orientation, length)) {
			ship.place(coords, orientation);
			shipsPlaced.add(ship);
			switch (orientation) {
			case Ship.NORTH:
				if (coords.getY() < 9) {
					field[coords.getY() + 1][coords.getX()] = PUFFER;
				}
				for (int i = 0; i < length; i++) {
					field[coords.getY() - i][coords.getX()] = SHIP;
					if (coords.getX() < 9) {
						field[coords.getY() - i][coords.getX() + 1] = PUFFER;
					}
					if (coords.getX() > 0) {
						field[coords.getY() - i][coords.getX() - 1] = PUFFER;
					}
				}
				if (coords.getY() - length > 0) {
					field[coords.getY() - length][coords.getX()] = PUFFER;
				}
				break;
			case Ship.SOUTH:
				if (coords.getY() > 0) {

					field[coords.getY() - 1][coords.getX()] = PUFFER;
				}
				for (int i = 0; i < length; i++) {
					field[coords.getY() + i][coords.getX()] = SHIP;
					if (coords.getX() < 9) {
						field[coords.getY() + i][coords.getX() + 1] = PUFFER;
					}
					if (coords.getX() > 0) {
						field[coords.getY() + i][coords.getX() - 1] = PUFFER;
					}
				}
				if (coords.getY() + length < 9) {
					field[coords.getY() + length][coords.getX()] = PUFFER;
				}
				break;
			case Ship.EAST:
				if (coords.getX() > 0) {
					field[coords.getY()][coords.getX() - 1] = PUFFER;
				}
				for (int i = 0; i < length; i++) {
					field[coords.getY()][coords.getX() + i] = SHIP;
					if (coords.getY() < 9) {
						field[coords.getY() + 1][coords.getX() + i] = PUFFER;
					}
					if (coords.getY() > 0) {
						field[coords.getY() - 1][coords.getX() + i] = PUFFER;
					}
				}
				if (coords.getX() + length < 9) {
					field[coords.getY()][coords.getX() + length] = PUFFER;
				}
				break;
			case Ship.WEST:
				if (coords.getX() < 9) {
					field[coords.getY()][coords.getX() + 1] = PUFFER;
				}
				for (int i = 0; i < length; i++) {
					field[coords.getY()][coords.getX() - i] = SHIP;
					if (coords.getY() < 9) {
						field[coords.getY() + 1][coords.getX() - i] = PUFFER;
					}
					if (coords.getY() > 0) {
						field[coords.getY() - 1][coords.getX() - i] = PUFFER;
					}
				}
				if (coords.getX() - length > 0) {
					field[coords.getY()][coords.getX() - length] = PUFFER;
				}
				break;
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Testet, ob bei einem Schuss auf ein Feld ein Treffer auf ein Schiff
	 * vorliegt.
	 * @param coords Feld, welches getestet werden soll
	 * @return true, falls Schiff getroffen wurde, sonst false
	 */ 
	public boolean wasHit(intPair coords) {
		return (field[coords.getY()][coords.getX()] == SHIP);
	}

	/**
	 * Methode um herauszufinden, welches Schiff auf einem Feld liegt. 
	 * Noetig, da in Battlefield nur gespeichert wird WO ein Schiff liegt,
	 * jedoch nicht welches.
	 * @param coords Feld, welches geprueft wird
	 * @return Gibt das Schiff zurueck, welches an diesem Feld gefunden wurde
	 */
	public Ship getShipAt(intPair coords) {
		for (int i = 0; i < 10; i++) {
			if (shipsPlaced.get(i).isHit(coords)) {
				return shipsPlaced.get(i);
			}
		}
		System.out.println("not found!");
		return null;
	}
	
	public int getFieldStatus(int x, int y){
		return field[y][x];
	}

	/**
	 * Methode, welche ueberprueft, ob die Platzierung eines Schiffes moeglich
	 * und regelkonform ist.
	 * @param coords Startkoordinaten des Schiffes
	 * @param orientation Orientierung des Schiffes
	 * @param length Laenge des Schiffes
	 * @return true, falls Positionierung zulaessig ist, ansonsten false
	 */
	public boolean isValid(intPair coords, int orientation, int length) {
		boolean isValid = true;
		try {
			switch (orientation) {
			case Ship.NORTH:
				for (int i = 0; i < length; i++) {
					if (field[coords.getY() - i][coords.getX()] != WATER) {
						isValid = false;
						break;
					}
				}
				break;
			case Ship.SOUTH:
				for (int i = 0; i < length; i++) {
					if (field[coords.getY() + i][coords.getX()] != WATER) {
						isValid = false;
						break;
					}
				}
				break;
			case Ship.EAST:
				for (int i = 0; i < length; i++) {
					if (field[coords.getY()][coords.getX() + i] != WATER) {
						isValid = false;
						break;
					}
				}
				break;
			case Ship.WEST:
				for (int i = 0; i < length; i++) {
					if (field[coords.getY()][coords.getX() - i] != WATER) {
						isValid = false;
						break;
					}
				}
				break;

			}
		} catch (ArrayIndexOutOfBoundsException e) {
			isValid = false;
		}
		return isValid;
	}

	/**
	 * Methode, um auf ein Feld zu schiessen. Wird vom gegnerischen Spieler 
	 * aufgerufen.
	 * @param coords Feld, auf welches geschossen wird
	 * @return Gibt das Ergebniss des Schusses zurueck, also ob Wasser getroffen
	 * wurde, ein bereits beschossenes Feld, ein Schiff, oder ob ein Schiff
	 * sogar zerstoert wurde
	 */
	public Shots hit(intPair coords) {
		Shots status = Shots.SHOT_IMP;
		switch (field[coords.getY()][coords.getX()]) {
		case SHIP:
			Ship hittedOne = getShipAt(coords);
			hittedOne.hit();
			field[coords.getY()][coords.getX()] = SCRAP;
			status = Shots.SHOT_SHIP;
			if (hittedOne.isDestroyed()) {
				status = Shots.DEST_SHIP;
			}
			break;
		case WATER:
			status = Shots.SHOT_WATER;
			field[coords.getY()][coords.getX()] = WATERHIT;
			break;
		case PUFFER:
			status = Shots.SHOT_WATER;
			field[coords.getY()][coords.getX()] = WATERHIT;
			break;
		case SCRAP:
			status = Shots.SHOT_IMP;
			break;
		case WATERHIT:
			status = Shots.SHOT_IMP;
			break;
		}
		return status;
	}
	
	public String toString(){
		String str = "";
		
		for (int i=0; i<10; i++){
			for (int j=0; j<10; j++){
				str += field[i][j];
			}
			str += "\n";
		}
		
		return str;
	}
}
