package game;

import javax.swing.ImageIcon;
import java.io.File;

/**
 * Abstrakte Oberklasse zum speichern eines Schiffes.
 * @author Tobias Tomski
 */
public abstract class Ship {

	
	/**
	 * Laenge, Koordinaten und Ausrichtung eines Schiffes
	 */
	protected int length;
	protected intPair coords;
	protected int orientation;
	
	/**
	 * Variablen, um Anzahl der treffer zu speichern, sowie ob das Schiff
	 * zerstoert wurde
	 */
	protected boolean dest = false;
	protected int numberOfHits = 0;
	protected String name;

	/**
	 * Moegliche Ausrichtungen eines Schiffes
	 */
	public static final int NORTH = 1;
	public static final int SOUTH = -1;
	public static final int EAST = 2;
	public static final int WEST = -2;

	public Ship() {
		setLength();
		name = this.getClass().getSimpleName();
	}

	public intPair getCoords(){
		return coords;
	}
	
	public int getHitNum(){
		return numberOfHits;
	}
	
	public void setDest(boolean dest){
		this.dest = dest;
	}
	
	public void setNumOfHits(int numberOfHits){
		this.numberOfHits = numberOfHits;
	}
	
	/**
	 * Methode, falls ein Schiff getroffen wurde. Wird aus dem Schlachtfeld
	 * aufgerufen.
	 */
	public void hit() {
		if (!dest) {
			numberOfHits++;
			if (numberOfHits == length) {
				dest = true;
			}
		}
	}

	public boolean isDestroyed() {
		return dest;
	}

	public int getOrientation() {
		return this.orientation;
	}

	/**
	 * Prueft, ob ein Schuss auf eine Koordinate dieses Schiff treffen wuerde.
	 * Wird aus dem Schlachtfeld aufgerufen.
	 * @param coords Feld, welches beschossen wird
	 * @return Gibt true zurueck, falls das Schiff getroffen wird, sonst false
	 */
	public boolean isHit(intPair coords) {
		switch (this.orientation) {
		case NORTH:
			return (this.coords.getX() == coords.getX()
					&& coords.getY() >= this.coords.getY() - this.length && coords
					.getY() <= this.coords.getY());
		case SOUTH:
			return (this.coords.getX() == coords.getX()
					&& coords.getY() <= this.coords.getY() + this.length && coords
					.getY() >= this.coords.getY());
		case EAST:
			return (this.coords.getY() == coords.getY()
					&& coords.getX() <= this.coords.getX() + this.length && coords
					.getX() >= this.coords.getX());
		case WEST:
			return (this.coords.getY() == coords.getY()
					&& coords.getX() >= this.coords.getX() - this.length && coords
					.getX() <= this.coords.getX());
		default:
			return false;
		}
	}

	public abstract void setLength();

	/**
	 * Gibt das Bild zurueck, welches dem Schiff zugeordnet ist. Ist abhaengig
	 * von dem Typen des Schiffes. Benoetigt fuer die GUI.
	 * @return Imageicon
	 */
	public ImageIcon getImageIcon() {
		boolean notjar = (new File("../../src/gui/graphics/water.jpg"))
				.exists();

		if (notjar) {
			return new ImageIcon("../../src/gui/graphics/" + name + ".jpg");
		} else {
			return new ImageIcon(Ship.class.getResource("/gui/graphics/" + name
					+ ".jpg"));
		}
	}

	public int getLength() {
		return length;
	}

	/**
	 * Methode, um ein Schiff zu platzieren. Ordnet die Koordinaten und 
	 * die Orientierung zu.
	 * @param coords "Startkoordinaten" des Schiffes
	 * @param orientation Ausrichtung des Schiffes
	 */
	public void place(intPair coords, int orientation) {
		this.coords = coords;
		this.orientation = orientation;
	}

	public String toString() {
		return name;
	}
}
