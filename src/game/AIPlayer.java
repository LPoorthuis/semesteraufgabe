package game;

import java.util.Random;
import java.util.ArrayList;

/**
 * Computergesteuerter Spieler als Gegner, mit einer sehr einfachen Schwierig-
 * keit, da alle Schiffe zufaellig platziert und alle Felder zufaellig beschossen
 * werden.
 * @author Tobias Tomski
 */
public class AIPlayer extends Player {
	/**
	 * Speichert zu Beginn alle Felder. Sobald ein Feld beschossen wurde,
	 * wird es geloesch. Verhindert mehrfach-Beschuss eines Feldes.
	 */
	ArrayList<intPair> shots = new ArrayList<intPair>();
	ArrayList<intPair> tmp = new ArrayList<intPair>();
	
	/**
	 * Moegliche Felder, auf welchen ein Schiff platziert werden kann.
	 * Verhindert, dass auch besetzte Felder und Puffer mit einbezogen werden.
	 */
	ArrayList<intPair> possibleFields = new ArrayList<intPair>();

	public ArrayList<intPair> getShots(){
		return shots;
	}
	
	public AIPlayer() {
		myBf = new Battlefield();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				shots.add(new intPair(i, j));
				possibleFields.add(new intPair(i, j));
			}
		}
	}
	
	public AIPlayer(Battlefield myBf, int shipsDestroyed, ArrayList<intPair> shots){
		this.myBf = myBf;
		this.shipsDestroyed = shipsDestroyed;
		this.shots = shots;
	}

	/**
	 * Methode zum platzieren der Schiffe zu Beginn.Platziert die Schiffe 
	 * komplett zufaellig.
	 */
	public void placeShips() {
		Random rand = new Random();
		int[] possibleOrientations = { Ship.NORTH, Ship.SOUTH, Ship.WEST,
				Ship.EAST };
		ArrayList<Ship> Ships = Battlefield.getShips();
		int orientation = Ship.NORTH;
		int placedShips = 0;
		intPair coord;

		// TODO Schiffe platzieren und zu placedShips hinzufuegen
		// NICHT die Methode turn verwenden, ggf. neue Methode/Koordinatenliste
		// erstellen
		while (placedShips < 10) {
			int randomNum = rand.nextInt(possibleFields.size());
			coord = possibleFields.get(randomNum);
			boolean positionPossible = false;
			int counter = 0;
			while (!positionPossible && counter < 100) {
				orientation = possibleOrientations[rand.nextInt(4)];
				positionPossible = myBf.place(Ships.get(placedShips),
						coord, orientation);
				counter++;
			}
			if (counter != 100) {
				placedShips++;
				removeNotPossibleFields(coord, orientation,
						Ships.get(placedShips - 1).getLength());
			} else {
				possibleFields.remove(randomNum);
				int tries = 0;
				while (!positionPossible && tries < 4) {
					orientation = possibleOrientations[tries];
					positionPossible = myBf.place(
							Ships.get(placedShips), coord, orientation);
					if(positionPossible){
						placedShips++;
						removeNotPossibleFields(coord, orientation,
								Ships.get(placedShips - 1).getLength());
					}
					tries++;
				}
			}
		}
	}

	/**
	 * Loescht alle Felder, auf welche keine Schiffe mehr positioniert werden koennen,
	 * also alle auf den ein SChiff liegt, sowie dessen Pufferzonen.
	 * @param coord Startfeld des platzierten Schiffes
	 * @param orientation Orientierung des platzierten Schiffes
	 * @param length Laenge des platzierten Schiffes
	 */
	public void removeNotPossibleFields(intPair coord, int orientation,
			int length) {
		switch (orientation) {
		case Ship.NORTH:
			for(int i = 0; i < length; i++){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()-i));
				if(coord.getX() < 9){
					possibleFields.remove(new intPair(coord.getX()+1, coord.getY()-i));
				}
				if(coord.getX() > 0){
					possibleFields.remove(new intPair(coord.getX()-1, coord.getY()-i));
				}
			}
			if(coord.getY() < 9){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()+1));
			}
			if(coord.getY() - length >= 0){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()-length));
			}
			break;
		case Ship.SOUTH:
			for(int i = 0; i < length; i++){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()+i));
				if(coord.getX() < 9){
					possibleFields.remove(new intPair(coord.getX()+1, coord.getY()+i));
				}
				if(coord.getX() > 0){
					possibleFields.remove(new intPair(coord.getX()-1, coord.getY()+i));
				}
			}
			if(coord.getY() > 0){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()-1));
			}
			if(coord.getY() + length <= 9){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()+length));
			}
			break;
		case Ship.EAST:
			for(int i = 0; i < length; i++){
				possibleFields.remove(new intPair(coord.getX()+i, coord.getY()));
				if(coord.getY() < 9){
					possibleFields.remove(new intPair(coord.getX()+i, coord.getY()+1));
				}
				if(coord.getY() > 0){
					possibleFields.remove(new intPair(coord.getX()+i, coord.getY()-1));
				}
			}
			if(coord.getX() > 0){
				possibleFields.remove(new intPair(coord.getX()-1, coord.getY()));
			}
			if(coord.getX() + length <= 9){
				possibleFields.remove(new intPair(coord.getX()+length, coord.getY()));
			}
			break;
		case Ship.WEST:
			for(int i = 0; i < length; i++){
				possibleFields.remove(new intPair(coord.getX()-i, coord.getY()));
				if(coord.getY() < 9){
					possibleFields.remove(new intPair(coord.getX()-i, coord.getY()+1));
				}
				if(coord.getY() > 0){
					possibleFields.remove(new intPair(coord.getX()-i, coord.getY()-1));
				}
			}
			if(coord.getX() < 9){
				possibleFields.remove(new intPair(coord.getX()+1, coord.getY()));
			}
			if(coord.getX() - length >= 0){
				possibleFields.remove(new intPair(coord.getX()-length, coord.getY()));
			}
			break;
		}
	}

	/* (non-Javadoc)
	 * @see game.Player#turn()
	 */
	public intPair turn() {
		Random rand = new Random();
		int randomNum = rand.nextInt(shots.size());
		intPair turn = shots.get(randomNum);
		shots.remove(turn);
		System.out.println(turn);
		tmp.add(turn);
		System.out.println(myBf.toString());
		return turn;
	}
}
