package game;

/**
 * Exception, welche bei einer fehlerhaften Platzierung geschmissen wird.
 * @author Tobias Tomski
 */
public class PlacementException extends Exception {

	private static final long serialVersionUID = 1L;

	public PlacementException(String m) {
		super(m);
	}
	
	public PlacementException(){
		super();
	}
	
}
