package game;
import game.Shots;

/**
 * Abstrakte Klasse fuer den Spieler. Speichert das Schlachtfeld, und ermoeglicht
 * grundlegende Spiellogik, wie etwa platzieren oder schiessen. Dient als 
 * grundlegende Klasse fuer das ganze Spiel.
 * @author Tobias Tomski
 */
public abstract class Player {

	protected Shots[][] hitField = new Shots[10][10];
	protected boolean isLoser = false;
	protected Battlefield myBf;
	protected int shipsDestroyed = 0;

	public boolean isLoser() {
		return isLoser;
	}

	public int getNumOfDest(){
		return shipsDestroyed;
	}
	
	public abstract intPair turn();

	/**
	 * Schuss auf ein Feld, wird vom Gegner aufgerufen. Ruft die Schuss-Methode
	 * des Schlachtfeldes auf. Prueft gleichzeitig, ob man verloren hat oder nicht.
	 * @param coords Feld, auf welches geschossen wird
	 * @return Gibt den Status des Schusses zurueck. Leitet nur den Status der 
	 * Methode shoot in Battlefield weiter
	 */
	public Shots shoot(intPair coords) {
		Shots status = myBf.hit(coords);
		if (status == Shots.DEST_SHIP) {
			shipsDestroyed++;
		}
		if (shipsDestroyed == 10) {
			isLoser = true;
		}
		return status;
	}
	
	public Battlefield getBf(){
		return myBf;
	}
	
	public int getShipsDestroyed(){
		return shipsDestroyed;
	}

	public Shots getHitField(int x, int y) {
		return hitField[x][y];
	}

	public void setHitField(int x, int y, Shots elem) {
		this.hitField[x][y] = elem;
	}
	
	public Shots[][] getHit(){
		return this.hitField;
	}
}
