package game;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Diese Klasse dient dazu, ein laufendes Spiel zu speichern. Dabei werden alle Attribute aus den Objekten
 * ausgelesen und in einer Datei gespeichert. Beim laden koennen diese dann ausgelesen werden, sodass mit
 * den alten Werten neue Objekte erstellt werden koennen, welche dem alten Speicherstand entsprechen.
 * 
 * @author Tobias Tomski
 */

public class SaveGame{
	
	
	/**
	 * Hier werden die zu speichernden Objekte abgelegt. Aus diesen werden
	 * die Daten ausgelesen und in eine Datei geschrieben.
	 * Beim laden werden diese Daten dann wieder ausgelesen, und den jeweiligen
	 * Objekten zugewiesen. Diese koennen dann ausgelesen und im Spiel
	 * wieder zugewiesen bzw erzeugt werden.
	 */
	HumanPlayer humanPlayer;
	AIPlayer2 aiPlayer;
	Battlefield humanBf;
	Battlefield aiBf;
	int[][] humanField;
	int[][] aiField;
	Shots[][] humanHit;
	Shots[][] aiHit;
	ArrayList<Ship> humanPlaced;
	ArrayList<Ship> aiPlaced;
	gui.GameWindow gamewin;
	gui.SettingWindow setwin;
	
	/**
	 * Konstruktor, bekommst das GameWindow uebergeben. Sommit kann save() auf die Spieler zugreifen
	 * und deren Daten speichern. Das SettingWindow wird ebenfalls mit 
	 * uebergeben, sodass man mit speichern kann, ob animationen und/oder
	 * Sound aktiviert waren.
	 */
	public SaveGame(gui.GameWindow gamewin, gui.SettingWindow setwin){
		this.gamewin = gamewin;
		this.setwin = setwin;
	}

	/**
	 * Diese Methode speichert das Spiel in einer Datei 'savegame.sav'. Dabei werden die einzelnen
	 * Attribute der Objekte gespeichert, sodass diese spaeter wieder ausgelesen und den neu erstellten
	 * Objekten zugeordnet werden koennen. Dabei sieht stehen an oberster Stelle in der Datei die 
	 * Spielfelder, zunaechst das des humanPlayers, dann das des aiPlayers. Anschliessen werden die
	 * einzelnen Schiffe gespeichert, wieder zuerst der humanPlayer, dann der aiPlayer. Dabei steht 
	 * an erster Stelle die x-Koordinate, dann die y-Koordinate, danach die Orientierung, anschliessend
	 * ob das Schiff bereits zerstoert wurde, und zuletzt die Anzahl der Treffer, welche bereits auf
	 * das Schiff gelandet wurden.
	 * Zuletzt stehen noch die Anzahl der zerstoerten Schiffe der jeweiligen Spieler, wie gehabt zunaechst
	 * der humanPlayer, und anschliessend der aiPlayer, sowie darauf folgend die uebrigen moeglichen
	 * Schuesse der aiPlayers.
	 */
	public void save(){

		try{
			
			File save_file = new File("../../saves/savegame.sav");
			
			if(!save_file.exists()){
				try{
					save_file.createNewFile();
				}catch(IOException e){
					save_file = new File("saves/savegame.sav");
					if(!save_file.exists()){
						save_file.createNewFile();
					}
				}
			}
			
			FileWriter save = new FileWriter(save_file);
			save.write("");
			
			humanPlayer = gamewin.getHumanPlayer();
			aiPlayer = gamewin.getAiPlayer();
			
			humanBf = humanPlayer.getBf();
			aiBf = aiPlayer.getBf();
			
			humanPlaced = humanBf.getShipsPlaced();
			aiPlaced = aiBf.getShipsPlaced();
			
			humanField = humanBf.getField();
			aiField = aiBf.getField();
			
			humanHit = humanPlayer.getHit();
			aiHit = aiPlayer.getHit();
			
			for(int i = 0; i < humanField.length; i++){
				for(int j = 0; j < humanField[0].length; j++){
					save.append(String.valueOf(humanField[i][j]));
				}
				save.append("\n");
			}
			save.append("\n");
			
			for(int i = 0; i < aiField.length; i++){
				for(int j = 0; j < aiField[0].length; j++){
					save.append(String.valueOf(aiField[i][j]));
				}
				save.append("\n");
			}
			save.append("\n");

			for(int i = 0; i < humanHit.length; i++){
				for(int j = 0; j < humanHit[0].length; j++){
					switch(humanHit[i][j]){
					case SHOT_SHIP:
						save.append("1");
						break;
					case DEST_SHIP:
						save.append("2");
						break;
					case SHOT_IMP:
						save.append("3");
						break;
					case SHOT_WATER:
						save.append("4");
						break;
					case NOT:
						save.append("5");
						break;
					default:
						break;
					}
				}
				save.append("\n");
			}
			save.append("\n");
			
			for(int i = 0; i < aiHit.length; i++){
				for(int j = 0; j < aiHit[0].length; j++){
					switch(aiHit[i][j]){
					case SHOT_SHIP:
						save.append("1");
						break;
					case DEST_SHIP:
						save.append("2");
						break;
					case SHOT_IMP:
						save.append("3");
						break;
					case SHOT_WATER:
						save.append("4");
						break;
					case NOT:
						save.append("5");
						break;
					default:
						break;
					}
				}
				save.append("\n");
			}
			save.append("\n");
			
			Iterator<Ship> it = humanPlaced.iterator();
			while(it.hasNext()){
				Ship buf = (Ship) it.next();
				save.append(String.valueOf(buf.getCoords().getX()));
				save.append(String.valueOf(buf.getCoords().getY()));
				if(buf.orientation > 0){
					save.append("+" + String.valueOf(buf.getOrientation()));
				}else{
					save.append(String.valueOf(buf.getOrientation()));
				}
				if(buf.isDestroyed()){
					save.append('d');
				}else{
					save.append('a');
				}
				save.append(String.valueOf(buf.getHitNum()));
				save.append("\n");
			}
			save.append("\n");
			
			it = aiPlaced.iterator();
			while(it.hasNext()){
				Ship buf = (Ship) it.next();
				save.append(String.valueOf(buf.getCoords().getX()));
				save.append(String.valueOf(buf.getCoords().getY()));
				if(buf.orientation > 0){
					save.append("+" + String.valueOf(buf.getOrientation()));
				}else{
					save.append(String.valueOf(buf.getOrientation()));
				}
				if(buf.isDestroyed()){
					save.append('d');
				}else{
					save.append('a');
				}
				save.append(String.valueOf(buf.getHitNum()));
				save.append("\n");
			}
			save.append("\n");
			
			save.append(String.valueOf(humanPlayer.getNumOfDest()) + "\n");
			save.append(String.valueOf(aiPlayer.getNumOfDest()) + "\n");
			save.append("\n");
			
			ArrayList<intPair> shots = ((AIPlayer2) aiPlayer).getShots();
			save.append(shots.size() + "\n");
			
			for(int i = 0; i < shots.size(); i++){
				save.append(String.valueOf(shots.get(i).getX()));
				save.append(String.valueOf(shots.get(i).getY()) + "\n");
			}
			save.append("\n");
			
			if(setwin.getAnimated()){
				save.append("1\n");
			}else{
				save.append("0\n");
			}
			
			if(setwin.getSound()){
				save.append("1\n");
			}else{
				save.append("0\n");
			}
			
			save.flush();
			save.close();
			
			
			
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Diese Methode laedt das Spiel, wobei vorher getestet wird ob eine Speicherdatei vorhanden ist.
	 * Dabei gibt die Methode 'true' zurueck, wenn das laden gelungen ist, und 'false', falls dies nicht
	 * der Fall ist.
	 * Fuer naehere Informationen wie die Daten gespeichert sind siehe save().
	 */
	public boolean load(){
		boolean possible = true;
		try{
			
			File load_file = new File("../../saves/savegame.sav");
		
			if(!load_file.exists()){
				load_file = new File("saves/savegame.sav");
			}
		
			FileReader load = new FileReader(load_file);
			
			if(load_file.exists()){
				System.out.println("file exists.");
				humanField = new int[10][10];
				aiField = new int[10][10];
			
				aiPlaced = Battlefield.getShips();
				humanPlaced = Battlefield.getShips();
				
				Shots[][] aiHitField = new Shots[10][10];
				Shots[][] humanHitField = new Shots[10][10];
			
				for(int i = 0; i < humanField.length; i++){
					for(int j = 0; j < humanField[0].length; j++){
						humanField[i][j] = Integer.parseInt(Character.toString((char)load.read()));
					}
					load.read();
				}
				load.read();
				
				for(int i = 0; i < aiField.length; i++){
					for(int j = 0; j < aiField[0].length; j++){
						aiField[i][j] = Integer.parseInt(Character.toString((char)load.read()));
					}
					load.read();
				}
				load.read();
				
				for(int i = 0; i < humanHitField.length; i++){
					for(int j = 0; j < humanHitField[0].length; j++){
						switch(Integer.parseInt(Character.toString((char)load.read()))){
						case 1:
							humanHitField[i][j] = Shots.SHOT_SHIP;
							break;
						case 2:
							humanHitField[i][j] = Shots.DEST_SHIP;
							break;
						case 3:
							humanHitField[i][j] = Shots.SHOT_IMP;
							break;
						case 4:
							humanHitField[i][j] = Shots.SHOT_WATER;
							break;
						case 5:
							humanHitField[i][j] = Shots.NOT;
							break;
						default:
							break;
						}
					}
					load.read();
				}
				load.read();
				
				for(int i = 0; i < aiHitField.length; i++){
					for(int j = 0; j < aiHitField[0].length; j++){
						switch(Integer.parseInt(Character.toString((char)load.read()))){
						case 1:
							aiHitField[i][j] = Shots.SHOT_SHIP;
							break;
						case 2:
							aiHitField[i][j] = Shots.DEST_SHIP;
							break;
						case 3:
							aiHitField[i][j] = Shots.SHOT_IMP;
							break;
						case 4:
							aiHitField[i][j] = Shots.SHOT_WATER;
							break;
						case 5:
							aiHitField[i][j] = Shots.NOT;
							break;
						default:
							break;
						}
					}
					load.read();
				}
				load.read();
				
				for(int i = 0; i < 10; i++){
					int x = Integer.parseInt(Character.toString((char)load.read()));
					int y = Integer.parseInt(Character.toString((char)load.read()));
					String pre = Character.toString((char) load.read());
					int num = Integer.parseInt(Character.toString((char)load.read()));
					int orientation;
					if(pre.equals("-")){
						orientation = -1*num;
					}else{
						orientation = num;
					}
					humanPlaced.get(i).place(new intPair(x, y), orientation);
					if(Character.toString((char) load.read()).equals("d")){		
						humanPlaced.get(i).setDest(true);
					}else{
						humanPlaced.get(i).setDest(false);
					}
					humanPlaced.get(i).setNumOfHits(Integer.parseInt(Character.toString((char)load.read())));
					load.read();
				}
				load.read();
				
				for(int i = 0; i < 10; i++){
					int x = Integer.parseInt(Character.toString((char)load.read()));
					int y = Integer.parseInt(Character.toString((char)load.read()));
					String pre = Character.toString((char) load.read());
					int num = Integer.parseInt(Character.toString((char)load.read()));
					int orientation;
					if(pre.equals("-")){
						orientation = -1*num;
					}else{
						orientation = num;
					}
					aiPlaced.get(i).place(new intPair(x, y), orientation);
					if(Character.toString((char) load.read()).equals("d")){
						aiPlaced.get(i).setDest(true);
					}else{
						aiPlaced.get(i).setDest(false);
					}
					aiPlaced.get(i).setNumOfHits(Integer.parseInt(Character.toString((char)load.read())));
					load.read();
				}
				load.read();
				
				int humanDest = Integer.parseInt(Character.toString((char)load.read()));
				load.read();
				int aiDest = Integer.parseInt(Character.toString((char)load.read()));
				load.read();
				load.read();
				ArrayList<intPair> Shots = new ArrayList<intPair>();
				StringBuffer sizeStr = new StringBuffer();
				sizeStr.append(Character.toString((char)load.read()));
				String c = Character.toString((char)load.read());
				if(!c.equals("\n")){
					sizeStr.append(c);
					c = Character.toString((char)load.read());
					if(!c.equals("\n")){
						sizeStr.append(c);
						load.read();
					}
				}
				int size = Integer.parseInt(sizeStr.toString());
				//load.read();
				int x, y;
				while(size > 0){
					x = Integer.parseInt(Character.toString((char)load.read()));
					y = Integer.parseInt(Character.toString((char)load.read()));
					intPair coord = new intPair(x, y);
					Shots.add(coord);
					size--;
					load.read();
				}

				humanBf = new Battlefield(humanField, humanPlaced);
				aiBf = new Battlefield(aiField, aiPlaced);
				
				humanPlayer = new HumanPlayer(humanBf, humanDest, humanHitField);
				aiPlayer = new AIPlayer2(aiBf, aiDest, Shots, aiHitField);
				
				load.read();
				int animations = Integer.parseInt(Character.toString((char) load.read()));
				load.read();
				int sounds = Integer.parseInt(Character.toString((char) load.read()));
				if(animations==1){
					setwin.setAnimated(true);
				}
				if(sounds==1){
					setwin.setSound(true);
				}
				
				possible = true;
				load.close();
			}
		} catch (Exception e) {
			possible = false;
		}
		return possible;
	}
	
	/**
	 * Methode, um die Speicherdatei zu resetten, das heisst deren Inhalt zu
	 * loeschen. Nur moeglich wenn Datei existiert. 
	 * Gibt 'true' zurueck wenn Datei resettet wurde, und 'false', falls keine
	 * Datei vorhanden oder diese leer war.
	 */
	//penis
	public boolean resetSave(){
		File file = new File("../../saves/savegame.sav");
		
		if(!file.exists()){
			file = new File("saves/savegame.sav");
		}
		
		try {
			FileWriter res = new FileWriter(file);
			res.write("");
			res.close();
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	/**
	 * Gibt den humanPlayer zurueck. Benoetigt zum erneuten initialisieren
	 * der Spieler mit den alten Werten nach dem laden.
	 */
	public HumanPlayer getHumanPlayer(){
		return humanPlayer;
	}
	
	/**
	 * Gibt den aiPlayer zurueck. Benoetigt zum erneuten initialisieren
	 * der Spieler mit den alten Werten nach dem laden.
	 */
	public AIPlayer2 getAiPlayer(){
		return aiPlayer;
	}
}