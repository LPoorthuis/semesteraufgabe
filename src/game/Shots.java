package game;

/**
 * Enum fuer den Status, welchen ein Schuss auf ein Feld zurueck gibt.
 * @author Tobias Tomski
 */
public enum Shots {
	SHOT_IMP, SHOT_WATER, SHOT_SHIP, DEST_SHIP, NOT;
}