package game;

/**
 * Klasse fuer den menschlichen Spieler. Speichert das Schlachtfeld, und er-
 * moeglicht Schuesse usw.
 * @author Tobias Tomski
 */
public class HumanPlayer extends Player {

	public HumanPlayer() {
		myBf = new Battlefield();
		
		for (int i=0; i<10; i++){
			for (int j=0; j<10; j++){
				setHitField(i, j, Shots.NOT) ;
			}
		}
	}
	
	/**
	 * Spezieller Konstruktor, falls ein Speicherstand geladen wird.
	 * @param myBf Eigenes Schlachtfeld, aus Datei gelesen
	 * @param shipsDestroyed Anzahle bereits zerstoerter Schiffe
	 * @param humanHitField 
	 */
	public HumanPlayer(Battlefield myBf, int shipsDestroyed, Shots[][] humanHitField){
		this.myBf = myBf;
		this.shipsDestroyed = shipsDestroyed;
		this.hitField = humanHitField;
	}

	public boolean place(Ship ship, intPair coords, int orientation) {
		return myBf.place(ship, coords, orientation);
	}

	public intPair turn() {
		return new intPair(0, 0);
	}
}
