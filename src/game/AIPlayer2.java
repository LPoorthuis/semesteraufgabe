package game;

import java.util.Collections;
import java.util.Random;
import java.util.ArrayList;

public class AIPlayer2 extends Player {
	ArrayList<intPair> shots = new ArrayList<intPair>();
	ArrayList<intPair> avShots = new ArrayList<intPair>();
	ArrayList<intPair> possibleFields = new ArrayList<intPair>();
	int[] orientationArr = {Ship.NORTH, Ship.EAST, Ship.SOUTH, Ship.EAST};
	int remCount = 3;

	public ArrayList<intPair> getShots(){
		return shots;
	}
	
	public AIPlayer2() {
		myBf = new Battlefield();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				shots.add(new intPair(i, j));
				possibleFields.add(new intPair(i, j));
			}
		}
		long seed = System.nanoTime();
		Collections.shuffle(shots, new Random(seed));
		
		for (int i=0; i<10; i++){
			for (int j=0; j<10; j++){
				setHitField(i, j, Shots.NOT) ;
			}
		}
	}

	public AIPlayer2(Battlefield myBf, int shipsDestroyed, ArrayList<intPair> shots, Shots[][] aiHitField){
		this.myBf = myBf;
		this.shipsDestroyed = shipsDestroyed;
		this.shots = shots;
		this.hitField = aiHitField;
	}
	
	public void placeShips() {
		Random rand = new Random();
		int[] possibleOrientations = { Ship.NORTH, Ship.SOUTH, Ship.WEST,
				Ship.EAST };
		ArrayList<Ship> Ships = Battlefield.getShips();
		int orientation = Ship.NORTH;
		int placedShips = 0;
		intPair coord;

		// TODO Schiffe platzieren und zu placedShips hinzufuegen
		// NICHT die Methode turn verwenden, ggf. neue Methode/Koordinatenliste
		// erstellen
		while (placedShips < 10) {
			int randomNum = rand.nextInt(possibleFields.size());
			coord = possibleFields.get(randomNum);
			boolean positionPossible = false;
			int counter = 0;
			while (!positionPossible && counter < 100) {
				orientation = possibleOrientations[rand.nextInt(4)];
				positionPossible = myBf.place(Ships.get(placedShips),
						coord, orientation);
				counter++;
			}
			if (counter != 100) {
				placedShips++;
				removeNotPossibleFields(coord, orientation,
						Ships.get(placedShips - 1).getLength());
			} else {
				possibleFields.remove(randomNum);
				int tries = 0;
				while (!positionPossible && tries < 4) {
					orientation = possibleOrientations[tries];
					positionPossible = myBf.place(
							Ships.get(placedShips), coord, orientation);
					if(positionPossible){
						placedShips++;
						removeNotPossibleFields(coord, orientation,
								Ships.get(placedShips - 1).getLength());
					}
					tries++;
				}
			}
		}
	}

	public void removeNotPossibleFields(intPair coord, int orientation, int length) {
		switch (orientation) {
		case Ship.NORTH:
			for(int i = 0; i < length; i++){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()-i));
				if(coord.getX() < 9){
					possibleFields.remove(new intPair(coord.getX()+1, coord.getY()-i));
				}
				if(coord.getX() > 0){
					possibleFields.remove(new intPair(coord.getX()-1, coord.getY()-i));
				}
			}
			if(coord.getY() < 9){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()+1));
			}
			if(coord.getY() - length >= 0){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()-length));
			}
			break;
		case Ship.SOUTH:
			for(int i = 0; i < length; i++){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()+i));
				if(coord.getX() < 9){
					possibleFields.remove(new intPair(coord.getX()+1, coord.getY()+i));
				}
				if(coord.getX() > 0){
					possibleFields.remove(new intPair(coord.getX()-1, coord.getY()+i));
				}
			}
			if(coord.getY() > 0){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()-1));
			}
			if(coord.getY() + length <= 9){
				possibleFields.remove(new intPair(coord.getX(), coord.getY()+length));
			}
			break;
		case Ship.EAST:
			for(int i = 0; i < length; i++){
				possibleFields.remove(new intPair(coord.getX()+i, coord.getY()));
				if(coord.getY() < 9){
					possibleFields.remove(new intPair(coord.getX()+i, coord.getY()+1));
				}
				if(coord.getY() > 0){
					possibleFields.remove(new intPair(coord.getX()+i, coord.getY()-1));
				}
			}
			if(coord.getX() > 0){
				possibleFields.remove(new intPair(coord.getX()-1, coord.getY()));
			}
			if(coord.getX() + length <= 9){
				possibleFields.remove(new intPair(coord.getX()+length, coord.getY()));
			}
			break;
		case Ship.WEST:
			for(int i = 0; i < length; i++){
				possibleFields.remove(new intPair(coord.getX()-i, coord.getY()));
				if(coord.getY() < 9){
					possibleFields.remove(new intPair(coord.getX()-i, coord.getY()+1));
				}
				if(coord.getY() > 0){
					possibleFields.remove(new intPair(coord.getX()-i, coord.getY()-1));
				}
			}
			if(coord.getX() < 9){
				possibleFields.remove(new intPair(coord.getX()+1, coord.getY()));
			}
			if(coord.getX() - length >= 0){
				possibleFields.remove(new intPair(coord.getX()-length, coord.getY()));
			}
			break;
		}
	}

	public intPair turn() {
		for (int i=0;i<6;i++){
			fixField();
		}
		cleanField();
		analAvShots();
		intPair turn;
		for (int i=0; i<10; i++){
			for (int j=0; j<10; j++){
				if (avShots.size() > 0){
					turn = avShots.get(0);
					avShots.remove(turn);
					shots.remove(turn);
					return turn;
				}
			}
		}
		turn = shots.get(0);
		while(getHitField(turn.getX(),turn.getY())!=Shots.NOT){
			shots.remove(turn);
			turn = shots.get(0);
		}
		shots.remove(turn);
		return turn;
	}

	private void fixField() {
		for (int i=0; i<10; i++){
			for (int j=0; j<10; j++){
				if (getHitField(i,j)==Shots.DEST_SHIP){
					int[][] test = {{i+1,j},{i,j-1},{i-1,j},{i,j+1}};
					
					for (int n=0; n<test.length;n++){
						if ((0<=test[n][0] && test[n][0]<10) && (0<=test[n][1] && test[n][1]<10)){
							if (getHitField(test[n][0], test[n][1])==Shots.SHOT_SHIP){
								setHitField(test[n][0], test[n][1], Shots.DEST_SHIP);
							}
						}
					}
				}
			}
		}
	}

	private void cleanField() {
		for (int i=0; i<10; i++){
			for (int j=0; j<10; j++){
				if (getHitField(i,j)==Shots.DEST_SHIP){
					if (i+1<10){
						if (!(getHitField(i+1,j)==Shots.DEST_SHIP || getHitField(i+1,j)==Shots.SHOT_SHIP))
							setHitField(i+1,j, Shots.SHOT_IMP);
					}
					if (i-1>=0){
						if (!(getHitField(i-1,j)==Shots.DEST_SHIP || getHitField(i-1,j)==Shots.SHOT_SHIP))
							setHitField(i-1,j, Shots.SHOT_IMP);
					}
					if (j+1<10){
						if (!(getHitField(i,j+1)==Shots.DEST_SHIP || getHitField(i,j+1)==Shots.SHOT_SHIP))
							setHitField(i,j+1, Shots.SHOT_IMP);
					}
					if (j-1>=0){
						if (!(getHitField(i,j-1)==Shots.DEST_SHIP || getHitField(i,j-1)==Shots.SHOT_SHIP))
							setHitField(i,j-1, Shots.SHOT_IMP);
					}
				}
			}
		}
	}

	private void analAvShots() {
		for (int i=0; i<10; i++){
			for (int j=0; j<10; j++){
				
				if (getHitField(i, j) == Shots.SHOT_SHIP){
					if (i+2<10){
						boolean vertP = getHitField(i+1,j)==Shots.SHOT_SHIP;
						if (vertP){
							if (getHitField(i+2, j)==Shots.NOT){
								avShots.add(new intPair(i+2, j));
								shots.remove(new intPair(i+2, j));
								return;
							}
						}
					}
					
					if (i-2>=0){
						boolean vertM = getHitField(i-1,j)==Shots.SHOT_SHIP;
						if (vertM){
							if (getHitField(i-2, j)==Shots.NOT){
								avShots.add(new intPair(i-2, j));
								shots.remove(new intPair(i-2, j));
								return;
							}
						}
					}
					
					if (j+2<10){
						boolean horP = getHitField(i,j+1)==Shots.SHOT_SHIP;
						if (horP){
							if (getHitField(i, j+2)==Shots.NOT){
								avShots.add(new intPair(i, j+2));
								shots.remove(new intPair(i, j+2));
								return;
							}
						}
					}
					
					if (j-2>=0){
						boolean horM = getHitField(i,j-1)==Shots.SHOT_SHIP;
						if (horM){
							if (getHitField(i, j-2)==Shots.NOT){
								avShots.add(new intPair(i, j-2));
								shots.remove(new intPair(i, j-2));
								return;
							}
						}
					}
				}
			}
		}
		
		for (int i=0; i<10; i++){
			for (int j=0; j<10; j++){
				if (getHitField(i,j)==Shots.SHOT_SHIP){
					int[][] test = {{i+1,j},{i,j-1},{i-1,j},{i,j+1}};
					shuffleArray(test);
					
					for (int n=0; n<test.length;n++){
						if ((0<=test[n][0] && test[n][0]<10) && (0<=test[n][1] && test[n][1]<10)){
							if (getHitField(test[n][0], test[n][1])==Shots.NOT){
								avShots.add(new intPair(test[n][0], test[n][1]));
								shots.remove(new intPair(test[n][0], test[n][1]));
								return;
							}
						}
					}
				}
			}
		}
		return;
	}
	
	static void shuffleArray(int[][] ar)
	  {
	    Random rnd = new Random();
	    for (int i = ar.length - 1; i > 0; i--)
	    {
	      int index = rnd.nextInt(i + 1);
	      // Simple swap
	      int[] a = ar[index];
	      ar[index] = ar[i];
	      ar[i] = a;
	    }
	  }
}
