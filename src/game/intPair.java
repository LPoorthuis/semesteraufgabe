package game;

/**
 * Klasse fuer den Datentyp intPair. Wird benoetigt um Koordianten effizient
 * zu speichern.
 * @author Tobias Tomski
 */
public class intPair {
	private int x;
	private int y;

	public intPair(int x, int y) {
		if (x <= 9 && x >= 0 && y <= 9 && y >= 0) {
			this.setX(x);
			this.setY(y);
		} else {
			try {
				throw new PlacementException("Coords " + x + "|" + y + " are not in Field.");
			} catch (PlacementException e) {
				e.printStackTrace();
			}
		}
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String toString() {
		return "(" + x + "/" + y + ")";
	}
	
	public boolean equals(intPair other){
		return (this.getX()==other.getX())&&(this.getY()==other.getY());
	}
}
