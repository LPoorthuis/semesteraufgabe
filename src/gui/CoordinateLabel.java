package gui;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Dies ist ein Label, welches sich zusaetzlich noch seine
 * Koordinaten in einem zweidimensionalen Feld merken kann.
 * 
 * @author jvalder
 *
 */
@SuppressWarnings("serial")
public class CoordinateLabel extends JLabel {

	/**
	 * Die Reihe, in der das Label steht
	 */
	private int row;
	/**
	 * Die Spalte, in der das Label steht
	 */
	private int column;

	/**
	 * Erzeugt ein Label mit Bild
	 * 
	 * @param i Die Reihe
	 * @param j Die Spalte 
	 * @param im Ein Bild, dass auf dem Label angezeigt wird
	 */
	public CoordinateLabel(int i, int j, ImageIcon im) {
		super(im);
		row = i;
		column = j;
	}

	/**
	 * @return Die Reihe, in der das Label steht
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @return Die Spalte, in der das Label steht
	 */
	public int getColumn() {
		return column;
	}

}
