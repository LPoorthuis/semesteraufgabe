package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Beschreibt das Einstellungsfenster fuer das Programm.
 * In diesem Fenster kann z.B. festgelegt werden, ob
 * im Spiel Animationen und Sounds abgespielt werden.
 * 
 * @author jvalder
 *
 */
@SuppressWarnings("serial")
public class SettingWindow extends JFrame {

	//GUI-Komponenten
	private JPanel panel1;
	private JButton save;
	private JButton cancel;
	private JCheckBox animations;
	private JCheckBox sounds;
	
	/**
	 * Gibt an, ob Animationen zur Zeit eingeschaltet sind
	 */
	private boolean animated = false;
	/**
	 * Gibt an, ob Sounds zur Zeit eingeschaltet sind
	 */
	private boolean sound = false;
	
	private JCheckBox fullscreen;
	
	/**
	 * Das zugehoerige Hauptfenster
	 */
	private MainWindow mainwin;
	
	
	/**
	 * Erzeugt ein Einstellungsfenster und uebergibt das
	 * zugehoerige Hauptfenster
	 * 
	 * @param mainwin Das zugehoerige Hauptfenster
	 */
	public SettingWindow(MainWindow mainwin) {
		this.mainwin = mainwin;
		initComponents();
		addListeners();
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	}

	/**
	 * Erzeugt alle Komponenten und erstellt das Layout des Fensters
	 */
	private void initComponents() {
		setTitle("Schiffe versenken - Einstellungen");
		// TODO Initialize Components
		setMinimumSize(new Dimension(300, 400));

		panel1 = new JPanel();
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		panel1.setAlignmentX(CENTER_ALIGNMENT);

		animations = new JCheckBox("Animationen einschalten");
		sounds = new JCheckBox("Soundeffekte einschalten");
		save = new JButton("Speichern");
		cancel = new JButton("Abbrechen");

		animations.setAlignmentX(CENTER_ALIGNMENT);
		sounds.setAlignmentX(CENTER_ALIGNMENT);
		fullscreen.setAlignmentX(CENTER_ALIGNMENT);
		save.setAlignmentX(CENTER_ALIGNMENT);
		cancel.setAlignmentX(CENTER_ALIGNMENT);

		panel1.add(Box.createRigidArea(new Dimension(0, 10)));
		panel1.add(animations);
		panel1.add(Box.createRigidArea(new Dimension(0, 10)));
		panel1.add(sounds);
		panel1.add(Box.createRigidArea(new Dimension(0, 10)));
		panel1.add(save);
		panel1.add(Box.createRigidArea(new Dimension(0, 10)));
		panel1.add(cancel);

		add(panel1);

		pack();
	}

	/**
	 * Erzeugt alle Actionlistener, die die Benutzereingaben verwalten
	 */
	private void addListeners() {
		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				animated = animations.isSelected();
				sound = sounds.isSelected();
				mainwin.setVisible(true);
				setVisible(false);
			}
		});

		cancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				animations.setSelected(animated);
				sounds.setSelected(sound);
				mainwin.setVisible(true);
				setVisible(false);

			}
		});
	}

	/**
	 * @return Gibt an, ob Animationen zur Zeit eingeschaltet sind
	 */
	public boolean getAnimated() {
		return animated;
	}

	/**
	 * @return Gibt an, ob Sounds zur Zeit eingeschaltet sind
	 */
	public boolean getSound() {
		return sound;
	}
	
	public void setAnimated(boolean animated){
		this.animated = animated;
	}
	
	public void setSound(boolean sound){
		this.sound = sound;
	}

}
