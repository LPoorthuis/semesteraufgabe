package gui;

import gui.GameWindow;
import gui.SettingWindow;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

/**
 * @author jvalder
 * Diese Klasse beschreibt das Hauptmenue des Programms.
 * Von hier aus kann ein neues Spiel gestartet, oder die
 * Einstellungen geoeffnet werden.
 */
@SuppressWarnings("serial")
public class MainWindow extends JFrame {

	// Component declaration - Start
	/**
	 * Das Spielefenster
	 */
	private GameWindow gamewin;
	/**
	 * Das Einstellungsfenster
	 */
	private SettingWindow setwin;
	/**
	 * GUI-Komponenten
	 */
	private JPanel mainPanel = new JPanel();
	private JButton newGame = new JButton();
	private JButton loadGame = new JButton();
	private JButton settings = new JButton();
	private JButton endGame = new JButton();
	
	/**
	 * Zeigt das Bild im Hauptmenue an
	 */
	private JLabel picLabel;

	// Component declaration - End

	/**
	 * Erzeugt das Hauptfenster, ein Spielefenster und ein Einstellungsfenster.
	 * Diese Komponenten bleiben bis zur Schliessung des Programms erhalten und
	 * werden nicht neu erzeugt.
	 */
	public MainWindow() {
		setwin = new SettingWindow(this);
		gamewin = new GameWindow(this, setwin);
		// Was soll bei Klick auf das System-X rechts oben passieren:
		// Das Programm soll beendet werden.
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		initComponents();
		addListeners();

	}

	/**
	 * Erstellt das Layout des Hauptmenues und initialisiert alle Komponenten
	 */
	private void initComponents() {
		setTitle("Schiffe versenken - Menue");
		mainPanel.setLayout(new javax.swing.BoxLayout(mainPanel,
				javax.swing.BoxLayout.Y_AXIS));

		mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		mainPanel.add(newGame);
		mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		mainPanel.add(loadGame);
		mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		mainPanel.add(settings);
		mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		mainPanel.add(endGame);
		mainPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		ImageIcon shippic;
		if ((new File("../../src/gui/graphics/ship.jpg")).exists()) {
			shippic = new ImageIcon("../../src/gui/graphics/ship.jpg");
		} else {
			shippic = new ImageIcon(
					MainWindow.class.getResource("/gui/graphics/ship.jpg"));
		}
		picLabel = new JLabel(new ImageIcon(shippic.getImage()
				.getScaledInstance(210, 210, java.awt.Image.SCALE_SMOOTH)));
		mainPanel.add(picLabel);

		newGame.setText("Neues Spiel");
		loadGame.setText("Spiel laden");
		settings.setText("Einstellungen");
		endGame.setText("Spiel beenden");

		mainPanel.setAlignmentX(CENTER_ALIGNMENT);
		newGame.setAlignmentX(CENTER_ALIGNMENT);
		loadGame.setAlignmentX(CENTER_ALIGNMENT);
		settings.setAlignmentX(CENTER_ALIGNMENT);
		endGame.setAlignmentX(CENTER_ALIGNMENT);
		picLabel.setAlignmentX(CENTER_ALIGNMENT);

		this.getContentPane().add(mainPanel);

		setMinimumSize(new Dimension(300, 400));

		gamewin.setVisible(false);

		pack();

	}

	/**
	 * Fuegt alle Actionlistener hinzu, die Benutzereingaben
	 * verwalten.
	 */
	private void addListeners() {
		newGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				gamewin.cleanUp();
				gamewin.resetSave();
				gamewin.setVisible(true);
				gamewin.startShipPlacingPhase();
				setVisible(false);

			}

		});

		loadGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean loadingSuccess = gamewin.loadGame(false);
				if(loadingSuccess){
					gamewin.setVisible(true);
				}
			}

		});

		settings.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setwin.setVisible(true);
				setVisible(false);
			}

		});

		endGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}

		});

	}

	/**
	 * Startet das Programm.
	 * 
	 * @param args Kommandozeilenargumente
	 */
	public static void main(String[] args) {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("Nimbus is not available");
		}

		MainWindow mw = new MainWindow();
		mw.setVisible(true);
	}
	
	public gui.GameWindow getGameWin(){
		return this.gamewin;
	}

}
