package gui;

import game.AIPlayer2;
import game.Battlefield;
import game.HumanPlayer;
import game.Ship;
import game.Shots;
import game.intPair;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

/**
 * Diese Klasse stellt das Spielefenster dar, welches die beiden Spielfelder
 * (naemlich das eigene und das des Gegners) enthaelt. Diese Klasse greift auf
 * die Spielelogik zu, um die Vorgaenge des Spiels darszustellen. Sie nimmt die
 * Zuege des Spielers an und leitet diese an die Spielelogik weiter.
 * 
 * @author Julia Valder
 *
 */
@SuppressWarnings("serial")
public class GameWindow extends JFrame {

	/**
	 * Das Spielfeld des Computergegners.
	 */
	private game.SaveGame save;
	private CoordinateLabel[][] field1 = new CoordinateLabel[10][10];
	/**
	 * Das Spielfeld des menschlichen Spielers.
	 */
	private CoordinateLabel[][] field2 = new CoordinateLabel[10][10];
	/**
	 * Buchstaben- und Zahlen-Beschriftungen fuer die Spielfelder
	 */
	private JLabel[] field1_num = new JLabel[10];
	private JLabel[] field1_alph = new JLabel[10];
	private JLabel[] field2_num = new JLabel[10];
	private JLabel[] field2_alph = new JLabel[10];
		
	/**
	 * Ein Label auf dem spielrelevante Informationen gezeigt
	 * werden (z.B. welches Schiff gerade platziert werden 
	 * soll, ob ein Schiff versenkt wurde, etc.)
	 */
	private JLabel shipLabel = new JLabel();
	/**
	 * Das Menue am oberen Rand des Fensters.
	 */
	private JMenuBar menubar;
	private JMenu menu_game;
	private JMenu menu_help;
	private JMenuItem newGame;
	private JMenuItem giveUp;
	private JMenuItem back;
	private JMenuItem showHelp;
	
	
	/**
	 * Panels, auf denen die Komponenten dargestellt werden.
	 * Verwalten die Layouts.
	 */
	private JPanel panel_lvl1_1 = new JPanel();
	private JPanel field1_pane = new JPanel();
	private JPanel field2_pane = new JPanel();
	private JPanel panel_lvl2_1 = new JPanel();
	private JPanel panel_lvl2_2 = new JPanel();
	
	
	/**
	 * Gibt an, ob der menschliche Spieler an der Reihe ist.
	 */
	private boolean humansTurn;
	/**
	 * Besagt, dass auf dieses Feld geschossen werden kann.
	 * Verarbeitet einen Schuss auf ein Feld.
	 */
	private MouseAdapter shootableField;
	/**
	 * Besagt, dass auf einem Feld ein Schiff platziert 
	 * werden kann. Verarbeitet einen Click auf dieses
	 * Feld und platziert ggf. ein Schiff.
	 */
	private MouseAdapter shipPlacing;
	/**
	 * Enthaelt relevante Daten des menschlichen Spielers,
	 * z.B. wo er Schiffe platziert hat.
	 */
	private HumanPlayer humanPlayer;
	/**
	 * Simuliert einen Computerspieler. Enthaelt alle
	 * relevanten Daten, z.B. wo Schiffe platziert sind
	 * und wohin der Computerspieler als naechstes 
	 * schiesst.
	 */
	private AIPlayer2 aiPlayer;
	
	
	/**
	 * Bilder und GIF's, die im Verlauf des Spiels angezeigt werden.
	 */
	private ImageIcon water;
	private ImageIcon waterShot;
	private ImageIcon shotAnimation;
	private ImageIcon shipShot;
	
	
	/**
	 * Gibt an, ob eine Animation angezeigt wird, wenn auf
	 * ein Feld geschossen wird.
	 */
	private boolean animated;
	/**
	 * Gibt an, ob ein Geraeusch abgespielt wird, wenn auf
	 * ein Feld geschossen wird.
	 */
	private boolean sound;	
	/**
	 * Das Hauptfenster (Hauptmenue).
	 */
	private MainWindow mainwin;
	/**
	 * Das Einstellungsfenster.
	 */
	private SettingWindow setwin;
	/**
	 * Das Schiff, das aktuell platziert werden soll.
	 */
	private Ship currentShip;
	/**
	 * Liste aller platzierbaren Schiffe.
	 */
	private ArrayList<Ship> shipList;

	/**
	 * Der Konstruktor initalisiert das Layout und die Komponenten, und erstellt
	 * ein Menue am oberen Rand des Fensters, damit der Spieler waehrend des
	 * Spiels ins Hauptmenue zurueckkehren kann.
	 */
	public GameWindow(MainWindow mainwin, SettingWindow setwin) {
		this.mainwin = mainwin;
		this.setwin = setwin;
		shipList = Battlefield.getShips();
		initImages();
		initAdapters();
		initComponents();
		createMenu();
		// TODO Start new game or load Game;
		humanPlayer = new HumanPlayer();
		aiPlayer = new AIPlayer2();
		save = new game.SaveGame(this, setwin);
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	}

	/**
	 * Diese Methode erzeugt alle Bilder, die im Laufe des Spiels
	 * verwendet werden. Die Bilder werden als globale Variablen
	 * gespeichert.
	 */
	private void initImages() {
		boolean notjar = (new File("../../src/gui/graphics/water.jpg"))
				.exists();

		if (notjar) {
			water = new ImageIcon("../../src/gui/graphics/water.jpg");
			waterShot = new ImageIcon("../../src/gui/graphics/waterShot.jpg");

			try {
				URL url = new URL(new URL("file:"),
						"../../src/gui/graphics/explosion.gif");
				shotAnimation = new ImageIcon(url);
			} catch (Exception e) {
				e.printStackTrace();
			}

			shipShot = new ImageIcon("../../src/gui/graphics/shipShot.jpg");
		} else {
			water = new ImageIcon(
					GameWindow.class.getResource("/gui/graphics/water.jpg"));
			waterShot = new ImageIcon(
					GameWindow.class.getResource("/gui/graphics/waterShot.jpg"));
			shotAnimation = new ImageIcon(
					GameWindow.class.getResource("/gui/graphics/explosion.gif"));
			shipShot = new ImageIcon(
					GameWindow.class.getResource("/gui/graphics/shipShot.jpg"));
		}
	}

	/**
	 * Erzeugt alle benoetigten GUI Komponenten und erzeugt das
	 * gewuenschte Layout fuer dieses Fenster.
	 */
	private void initComponents() {
		JLabel beschr1;
		JLabel beschr2;

		setTitle("Schiffe versenken - Spiel");
		setMinimumSize(new Dimension(1000, 570));
		panel_lvl1_1.setLayout(new javax.swing.BoxLayout(panel_lvl1_1,
				javax.swing.BoxLayout.Y_AXIS));

		// Hier werden die beiden Spielfelder erzeugt
		field1_pane = initField(field1_pane, field1, field1_num, field1_alph);
		field2_pane = initField(field2_pane, field2, field2_num, field2_alph);
		// Spielfelder - Ende
		
		beschr1 = new JLabel("Gegnerisches Feld");
		beschr1.setHorizontalAlignment(SwingConstants.RIGHT);
		
		Font font = beschr1.getFont();
		Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
		beschr1.setFont(boldFont);

		beschr2 = new JLabel("Dein Feld");
		beschr2.setHorizontalAlignment(SwingConstants.LEFT);
		beschr2.setFont(boldFont);
		
		shipLabel.setHorizontalAlignment(SwingConstants.CENTER);
		shipLabel.setBorder(BorderFactory.createEtchedBorder());

		panel_lvl2_1.setLayout(new java.awt.GridLayout(1, 3));
		panel_lvl2_2.setLayout(new javax.swing.BoxLayout(panel_lvl2_2,javax.swing.BoxLayout.X_AXIS));
		
		panel_lvl2_1.add(beschr2);
		panel_lvl2_1.add(shipLabel);
		panel_lvl2_1.add(beschr1);
		
		panel_lvl2_2.add(field2_pane);
		panel_lvl2_2.add(Box.createRigidArea(new Dimension(10, 0)));
		panel_lvl2_2.add(field1_pane);
		
		panel_lvl1_1.add(Box.createRigidArea(new Dimension(0, 10)));
		panel_lvl1_1.add(panel_lvl2_1);
		panel_lvl1_1.add(Box.createRigidArea(new Dimension(0, 20)));
		panel_lvl1_1.add(panel_lvl2_2);
		

		add(panel_lvl1_1);

		pack();

	}

	/**
	 * Erzeugt benoetigte Maus-Adapter, die Clicks auf das Spielfeld
	 * verarbeiten. Es gibt einen fuer die Platzierungsphase und einen
	 * fuer die Schiessphase.
	 */
	private void initAdapters() {
		shootableField = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {

				if (me.getSource() instanceof CoordinateLabel) {
					CoordinateLabel cl = (CoordinateLabel) me.getSource();

					Icon ic = new ImageIcon();

					if (humansTurn) {
						int row = cl.getRow();
						int column = cl.getColumn();
						Shots shotFired = aiPlayer.shoot(new intPair(column,
								row));
						switch (shotFired) {
						case SHOT_IMP:
							return;
						case SHOT_WATER:
							ic = new ImageIcon(waterShot.getImage()
									.getScaledInstance(42, 40,
											java.awt.Image.SCALE_SMOOTH));
							shipLabel.setText("");
							break;
						case SHOT_SHIP:
							ic = new ImageIcon(shipShot.getImage()
									.getScaledInstance(42, 40,
											java.awt.Image.SCALE_SMOOTH));
							shipLabel.setText("");
							break;
						case DEST_SHIP:
							ic = new ImageIcon(shipShot.getImage()
									.getScaledInstance(42, 40,
											java.awt.Image.SCALE_SMOOTH));
							field1[row][column].setIcon(ic);
							shipLabel.setText("Du hast ein Schiff versenkt!");
							break;
						default:
							break;
						}
						if (animated) {
							field1[row][column]
									.setIcon(new ImageIcon(
											shotAnimation
													.getImage()
													.getScaledInstance(
															42,
															40,
															java.awt.Image.SCALE_SMOOTH)));
							if (sound)
								playSound();

						}

						if (animated){
							final Icon ic2 = ic;
							final int row2 = row;
							final int column2 = column;
							Timer tm = new Timer();
							tm.schedule(new TimerTask() {
	
								@Override
								public void run() {
									field1[row2][column2].setIcon(ic2);
	
								}
							}, 1000);
						} else {
							field1[row][column].setIcon(ic);
						}

						saveGame();
						humansTurn = false;
						if (aiPlayer.isLoser()) {
							JOptionPane.showMessageDialog(cl.getRootPane(),
									"Du hast gewonnen!");
							save.resetSave();
							removeAllListeners();
						} else {
							aiPlayersTurn();
						}
					}
				}
			}
		};

		shipPlacing = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				if (me.getSource() instanceof CoordinateLabel) {
					CoordinateLabel cl = (CoordinateLabel) me.getSource();
					// Dialog anzeigen
					// Cancel = 0, North = 1, South = 2, East=3, West=4
					String options[] = { "Cancel", "North", "South", "East",
							"West" };
					int opt = JOptionPane.showOptionDialog(cl.getRootPane(),
							null, "Welche Richtung?",
							JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.INFORMATION_MESSAGE, null, options, 3);

					if (opt != 0) {
						opt += 1;
						opt = (int) ((opt / 2) * Math.pow(-1, opt % 2));
						boolean worked = humanPlayer.place(currentShip,
								new intPair(cl.getColumn(), cl.getRow()), opt);
						if (worked) {
							for (int i = 0; i < currentShip.getLength(); i++) {
								switch (opt) {
								case Ship.NORTH:
									field2[cl.getRow() - i][cl.getColumn()]
											.setIcon(new ImageIcon(
													currentShip
															.getImageIcon()
															.getImage()
															.getScaledInstance(
																	42,
																	40,
																	java.awt.Image.SCALE_SMOOTH)));
									break;
								case Ship.SOUTH:
									field2[cl.getRow() + i][cl.getColumn()]
											.setIcon(new ImageIcon(
													currentShip
															.getImageIcon()
															.getImage()
															.getScaledInstance(
																	42,
																	40,
																	java.awt.Image.SCALE_SMOOTH)));
									break;
								case Ship.EAST:
									field2[cl.getRow()][cl.getColumn() + i]
											.setIcon(new ImageIcon(
													currentShip
															.getImageIcon()
															.getImage()
															.getScaledInstance(
																	42,
																	40,
																	java.awt.Image.SCALE_SMOOTH)));
									break;
								case Ship.WEST:
									field2[cl.getRow()][cl.getColumn() - i]
											.setIcon(new ImageIcon(
													currentShip
															.getImageIcon()
															.getImage()
															.getScaledInstance(
																	42,
																	40,
																	java.awt.Image.SCALE_SMOOTH)));
									break;
								}
							}
							shipList.remove(currentShip);
							if (shipList.size() == 0) {
								endShipPlacingPhase();
								shipLabel.setText("");
							} else {
								currentShip = shipList.get(0);
								shipLabel.setText("Aktuelles Schiff: "
										+ currentShip.toString() + ", Laenge: "
										+ currentShip.getLength());
							}
						}
					}
				}

			}
		};

	}

	/**
	 * Start die Platzierungsphase des Spiels, indem entsprechende
	 * Maus-Adapter zum Spielfeld hinzugefuegt werden.
	 */
	public void startShipPlacingPhase() {

		for (int i = 0; i < field2.length; i++) {
			for (int j = 0; j < field2[0].length; j++) {
				field2[i][j].addMouseListener(shipPlacing);
			}
		}
    	
    	this.animated = setwin.getAnimated();
    	this.sound = setwin.getSound();
    	
    	aiPlayer.placeShips();
    	//
    	if (shipList.size()>0) currentShip = shipList.get(0);
		shipLabel.setText("Aktuelles Schiff: "+currentShip.toString()+", Laenge: "+currentShip.getLength());
    	
    	JOptionPane.showMessageDialog(this, "Das Spiel startet nun, bitte platziere deine Schiffe!");
    	
    }

	/**
	 * Beendet die Platzierungsphase des Spiels, indem entsprechende 
	 * Maus-Adapter ausgetauscht werden.
	 */
	private void endShipPlacingPhase() {

		for (int i = 0; i < field2.length; i++) {
			for (int j = 0; j < field2[0].length; j++) {
				field2[i][j].removeMouseListener(shipPlacing);
			}
		}

		for (int i = 0; i < field1.length; i++) {
			for (int j = 0; j < field1[0].length; j++) {
				field1[i][j].addMouseListener(shootableField);
			}
		}

		JOptionPane.showMessageDialog(this,
				"Du hast deine Schiffe platziert, das Spiel wird jetzt gestartet!");
		
		humansTurn = true;
		
		saveGame();
	}

	/**
	 * Entfernt alle Maus-Adapter von den Spielfeldern.
	 */
	private void removeAllListeners() {
		for (int i = 0; i < field2.length; i++) {
			for (int j = 0; j < field2[0].length; j++) {
				field2[i][j].removeMouseListener(shipPlacing);
			}
		}

		for (int i = 0; i < field1.length; i++) {
			for (int j = 0; j < field1[0].length; j++) {
				field1[i][j].removeMouseListener(shootableField);
			}
		}
	}

	/**
	 * @param field_pane
	 *            Das Panel, auf dem das Spielfeld dargestellt wird
	 * @param field
	 *            Ein Feld von CoordinateLabeln, das die Wasserfelder enthaelt
	 * @param field_num
	 *            Dieses Feld enthaelt die Zahlen-Labels zur Beschriftung der
	 *            y-Achse
	 * @param field_alph
	 *            Dieses Feld enthaelt die Buchstaben-Labels zur Beschriftung
	 *            der x-Achse
	 * @param clickable
	 *            Gibt an ob die Wasserfelder anclickbar sein sollen oder nicht
	 * @return Gibt das fertige Spielfeld als Panel zuruek
	 */
	private JPanel initField(JPanel field_pane, CoordinateLabel[][] field,
			JLabel[] field_num, JLabel[] field_alph) {
		// Ein Feld besteht aus einer 11x11 Tabelle
		field_pane.setLayout(new GridLayout(11, 11));
		JLabel one = new JLabel("/");
		one.setHorizontalAlignment(JLabel.CENTER);
		field_pane.add(one);

		int num = 1;
		char alph = 'A';

		// Zunaechst werden die Buchstaben Labels am oberen Rand des Feldes
		// hinzugefuegt.
		for (int i = 0; i < field_num.length; i++) {
			field_alph[i] = new JLabel("" + alph);
			field_alph[i]
					.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			field_alph[i].setHorizontalAlignment(JLabel.CENTER);
			alph++;
			field_pane.add(field_alph[i]);
		}

		// Hier werden die Zahlen-Labels am rechten Rand und die eigentlichen
		// Spielfelder hinzugefuegt.
		for (int i = 0; i < field_num.length; i++) {
			// Jede Reihe erhaelt zu Beginn ein Zahlen-Label
			if (num >= 10)
				field_num[i] = new JLabel("" + num);
			else
				field_num[i] = new JLabel("0" + num);
			field_num[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
			field_num[i].setHorizontalAlignment(JLabel.CENTER);
			num++;
			field_pane.add(field_num[i]);
			// Ende Zahlen-Label

			// Nun werden die Wasser-Feld initalisiert
			for (int j = 0; j < field1.length; j++) {

				field[i][j] = new CoordinateLabel(i, j, new ImageIcon(water
						.getImage().getScaledInstance(42, 40,
								java.awt.Image.SCALE_SMOOTH)));
				field[i][j].setBorder(BorderFactory
						.createLineBorder(Color.BLACK));

				field_pane.add(field[i][j]);
				// Ende Hinzufuegen des Wasserfeldes
			}

		}

		return field_pane;
	}

	/**
	 * Diese Methode koordiniert den Zug des Computerspielers. Sie holt sich
	 * seinen Zug und fuehrt entsprechende Operationen auf dem Bildschirm aus
	 * (z.B. Trefferanimation anzeigen).
	 */

	/**
	 * Simuliert einen Zug des Computerspielers. Dieser wird erst 
	 * nach 1,5 Sekunden ausgefuehrt, um dem menschlichen Spieler
	 * Zeit zu geben, den Zug zu verarbeiten.
	 */
	private void aiPlayersTurn() {

		final JFrame th = this;
		Timer tm2 = new Timer();
		tm2.schedule(new TimerTask() {

			@Override
			public void run() {
				intPair aiturn = aiPlayer.turn();
				int row = aiturn.getX();
				int column = aiturn.getY();
				Shots shotFired = humanPlayer.shoot(new intPair(column, row));
				Icon ic = new ImageIcon();
				switch (shotFired) {
				case SHOT_IMP:
					return;
				case SHOT_WATER:
					ic = new ImageIcon(waterShot.getImage().getScaledInstance(
							42, 40, java.awt.Image.SCALE_SMOOTH));
					shipLabel.setText("");
					aiPlayer.setHitField(row, column, shotFired);
					break;
				case SHOT_SHIP:
					ic = new ImageIcon(shipShot.getImage().getScaledInstance(
							42, 40, java.awt.Image.SCALE_SMOOTH));
					shipLabel.setText("");
					aiPlayer.setHitField(row, column, shotFired);
					break;
				case DEST_SHIP:
					ic = new ImageIcon(shipShot.getImage().getScaledInstance(
							42, 40, java.awt.Image.SCALE_SMOOTH));
					field2[row][column].setIcon(ic);
					shipLabel.setText("Dein Schiff wurde versenkt!");
					aiPlayer.setHitField(row, column, shotFired);
					break;
				default:
					break;
				}

				if (animated) {
					field2[row][column].setIcon(new ImageIcon(shotAnimation
							.getImage().getScaledInstance(42, 40,
									java.awt.Image.SCALE_SMOOTH)));
					if (sound)
						playSound();

				}

				final Icon ic2 = ic;
				final int row2 = row;
				final int column2 = column;
				Timer tm = new Timer();
				tm.schedule(new TimerTask() {

					@Override
					public void run() {
						field2[row2][column2].setIcon(ic2);

					}
				}, 1000);

				if (humanPlayer.isLoser()) {
					JOptionPane.showMessageDialog(th, "Du hast verloren!");
					save.resetSave();
					removeAllListeners();
				}
				humansTurn = true;
				saveGame();
			}
		}, 1500);

	}

	/**
	 * Spielt ein Explosionsgeraeusch ab.
	 */
	private void playSound() {
		try {
			String expFile = "../../src/gui/Sounds/explosion.wav";
			if (!(new File(expFile).exists())){
				expFile = "src/gui/Sounds/explosion.wav";
			}
			InputStream in = new FileInputStream(expFile);

			// create an audiostream from the inputstream
			AudioStream audioStream = new AudioStream(in);

			// play the audio clip with the audioplayer class
			AudioPlayer.player.start(audioStream);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	public void cleanUp() {
		removeAllListeners();
		for (int i = 0; i < field1.length; i++) {
			for (int j = 0; j < field1[0].length; j++) {
				field1[i][j]
						.setIcon(new ImageIcon(water.getImage()
								.getScaledInstance(42, 40,
										java.awt.Image.SCALE_SMOOTH)));
				field2[i][j]
						.setIcon(new ImageIcon(water.getImage()
								.getScaledInstance(42, 40,
										java.awt.Image.SCALE_SMOOTH)));
			}
		}
		humanPlayer = new HumanPlayer();
		aiPlayer = new AIPlayer2();
		shipList = Battlefield.getShips();
	}

	/**
	 * Diese Methode erstellt das Menue am oberen Fensterrand
	 */
	private void createMenu() {
		final JFrame th = this;
		menubar = new JMenuBar();

		// TODO Click Listener hinzufuegen

		menu_game = new JMenu("Game");
		menu_game.setMnemonic(KeyEvent.VK_G);
		menubar.add(menu_game);

		newGame = new JMenuItem("Start new game...", KeyEvent.VK_N);
		newGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cleanUp();
				resetSave();
				startShipPlacingPhase();
			}
		});
		menu_game.add(newGame);

		giveUp = new JMenuItem("Give up", KeyEvent.VK_U);
		giveUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int opt = JOptionPane.showConfirmDialog(th,
						"Wirklich aufgeben?", "Aufgeben",
						JOptionPane.YES_NO_OPTION);
				if (opt == 0) {
					JOptionPane.showMessageDialog(th, "Du hast verloren!");
					resetSave();
					removeAllListeners();
				}
			}
		});
		menu_game.add(giveUp);

		back = new JMenuItem("Back to main menu", KeyEvent.VK_B);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cleanUp();
				mainwin.setVisible(true);
				setVisible(false);
			}
		});
		menu_game.add(back);

		menu_help = new JMenu("Help");
		menu_help.setMnemonic(KeyEvent.VK_H);
		menubar.add(menu_help);

		showHelp = new JMenuItem("Show help", KeyEvent.VK_S);
		menu_help.add(showHelp);

		setJMenuBar(menubar);

		pack();
	}

	/**
	 * Speichert das Spiel, sodass es spaeter geladen werden kann.
	 */
	public void saveGame() {
		save.save();
	}
	
	/**
	 * Liest ein gespeichertes Spiel ein und startet es.
	 */
	public boolean loadGame(boolean Test) {
		boolean loaded = save.load();
		if(loaded){
			humanPlayer = save.getHumanPlayer();
			aiPlayer = save.getAiPlayer();
			ImageIcon ic;
		
			for (int i = 0; i < field1.length; i++) {
				for (int j = 0; j < field1[0].length; j++) {
					switch (aiPlayer.getBf().getFieldStatus(j, i)){
						case Battlefield.WATERHIT: ic = waterShot; break;
						case Battlefield.SCRAP: ic = shipShot; break;
						default: ic = water; break;
					}
				
					field1[i][j].setIcon(new ImageIcon(ic.getImage().getScaledInstance(
							42, 40, java.awt.Image.SCALE_SMOOTH)));
				}
			}
		
			for (int i = 0; i < field2.length; i++) {
				for (int j = 0; j < field2[0].length; j++) {
					switch (humanPlayer.getBf().getFieldStatus(j, i)){
						case Battlefield.SHIP: ic = humanPlayer.getBf().getShipAt(new intPair(j,i)).getImageIcon(); break;
						case Battlefield.WATERHIT: ic = waterShot; break;
						case Battlefield.SCRAP: ic = shipShot; break;
						default: ic = water; break;
					}
				
					field2[i][j].setIcon(new ImageIcon(ic.getImage().getScaledInstance(
							42, 40, java.awt.Image.SCALE_SMOOTH)));
				}
			}
		
			sound = setwin.getSound();
			animated = setwin.getAnimated();
		
			for (int i = 0; i < field1.length; i++) {
				for (int j = 0; j < field1[0].length; j++) {
					field1[i][j].addMouseListener(shootableField);
				}
			}
			if(!Test){
			JOptionPane.showMessageDialog(this,
					"Das Spiel wurde geladen!");
			}
			humansTurn = true;
		}else{
			JOptionPane.showMessageDialog(this,
					"Es ist kein gespeichertes Spiel vorhanden!");
		}
		return loaded;
	}
	
	public game.HumanPlayer getHumanPlayer(){
		return humanPlayer;
	}
	
	public void setHumanPlayer(game.HumanPlayer humanPlayer){
		this.humanPlayer = humanPlayer;
		this.aiPlayer = new game.AIPlayer2();
		aiPlayer.placeShips();
	}
	
	public AIPlayer2 getAiPlayer(){
		return aiPlayer;
	}
	
	public void resetSave(){
		save.resetSave();
	}
}
