package tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

public class BattlefieldTest {
	@SuppressWarnings("deprecation")
	@Test
	public void PlaceShipsTest(){
		game.HumanPlayer human = new game.HumanPlayer();
		ArrayList<game.Ship> shipList = game.Battlefield.getShips();
		human.place(shipList.get(0), new game.intPair(0,0), game.Ship.EAST);
		//wrong Placements
		human.place(shipList.get(1), new game.intPair(9,0), game.Ship.EAST);
		human.place(shipList.get(1), new game.intPair(0,1), game.Ship.EAST);
		human.place(shipList.get(1), new game.intPair(0,0), game.Ship.EAST);
		//correct Placements
		human.place(shipList.get(1), new game.intPair(0,2), game.Ship.EAST);
		human.place(shipList.get(2), new game.intPair(0,4), game.Ship.EAST);
		human.place(shipList.get(3), new game.intPair(0,6), game.Ship.EAST);
		human.place(shipList.get(4), new game.intPair(0,8), game.Ship.EAST);
		human.place(shipList.get(5), new game.intPair(6,2), game.Ship.EAST);
		human.place(shipList.get(6), new game.intPair(6,4), game.Ship.EAST);
		human.place(shipList.get(7), new game.intPair(6,6), game.Ship.EAST);
		human.place(shipList.get(8), new game.intPair(6,8), game.Ship.EAST);
		human.place(shipList.get(9), new game.intPair(9,0), game.Ship.SOUTH);
		int humanBf[][] = human.getBf().getField();
		
		//humanTestBf to compare humanBf with
		int humanTestBf[][] = new int[10][10];
		for(int i = 0; i < 10; i++){
			for(int j = 0; j < 10; j++){
				humanTestBf[i][j] = 0;
			}
		}
		//Battleship
		humanTestBf[0][0] = 1;
		humanTestBf[0][1] = 1;
		humanTestBf[0][2] = 1;
		humanTestBf[0][3] = 1;
		humanTestBf[0][4] = 1;
		humanTestBf[0][5] = 3;
		humanTestBf[1][0] = 3;
		humanTestBf[1][1] = 3;
		humanTestBf[1][2] = 3;
		humanTestBf[1][3] = 3;
		humanTestBf[1][4] = 3;
		//Cruiser
		humanTestBf[2][0] = 1;
		humanTestBf[2][1] = 1;
		humanTestBf[2][2] = 1;
		humanTestBf[2][3] = 1;
		humanTestBf[2][4] = 3;
		humanTestBf[3][0] = 3;
		humanTestBf[3][1] = 3;
		humanTestBf[3][2] = 3;
		humanTestBf[3][3] = 3;
		//Cruiser
		humanTestBf[4][0] = 1;
		humanTestBf[4][1] = 1;
		humanTestBf[4][2] = 1;
		humanTestBf[4][3] = 1;
		humanTestBf[4][4] = 3;
		humanTestBf[5][0] = 3;
		humanTestBf[5][1] = 3;
		humanTestBf[5][2] = 3;
		humanTestBf[5][3] = 3;
		//Destroyer
		humanTestBf[6][0] = 1;
		humanTestBf[6][1] = 1;
		humanTestBf[6][2] = 1;
		humanTestBf[6][3] = 3;
		humanTestBf[7][0] = 3;
		humanTestBf[7][1] = 3;
		humanTestBf[7][2] = 3;
		//Destroyer
		humanTestBf[8][0] = 1;
		humanTestBf[8][1] = 1;
		humanTestBf[8][2] = 1;
		humanTestBf[8][3] = 3;
		humanTestBf[9][0] = 3;
		humanTestBf[9][1] = 3;
		humanTestBf[9][2] = 3;
		//Destroyer
		humanTestBf[2][6] = 1;
		humanTestBf[2][7] = 1;
		humanTestBf[2][8] = 1;
		humanTestBf[2][9] = 3;
		humanTestBf[1][6] = 3;
		humanTestBf[1][7] = 3;
		humanTestBf[1][8] = 3;
		humanTestBf[3][6] = 3;
		humanTestBf[3][7] = 3;
		humanTestBf[3][8] = 3;
		humanTestBf[2][5] = 3;
		//Submarine
		humanTestBf[4][6] = 1;
		humanTestBf[4][7] = 1;
		humanTestBf[4][8] = 3;
		humanTestBf[5][6] = 3;
		humanTestBf[5][7] = 3;
		humanTestBf[4][5] = 3;
		//Submarine
		humanTestBf[6][6] = 1;
		humanTestBf[6][7] = 1;
		humanTestBf[6][8] = 3;
		humanTestBf[7][6] = 3;
		humanTestBf[7][7] = 3;
		humanTestBf[6][5] = 3;
		//Submarine
		humanTestBf[8][6] = 1;
		humanTestBf[8][7] = 1;
		humanTestBf[8][8] = 3;
		humanTestBf[9][6] = 3;
		humanTestBf[9][7] = 3;
		humanTestBf[8][5] = 3;
		//Submarine
		humanTestBf[0][9] = 1;
		humanTestBf[1][9] = 1;
		humanTestBf[0][8] = 3;
		humanTestBf[1][8] = 3;
		
		assertEquals(humanTestBf, humanBf);
		
		//Some Shots fired, compared to how it should look like
		human.shoot(new game.intPair(0,0));
		human.shoot(new game.intPair(1,0));
		human.shoot(new game.intPair(0,1));
		humanTestBf[0][0] = 2;
		humanTestBf[1][0] = 4;
		humanTestBf[0][1] = 2;
		
		assertEquals(0, human.getNumOfDest());
		
		human.shoot(new game.intPair(2,0));
		human.shoot(new game.intPair(3,0));
		human.shoot(new game.intPair(4,0));
		
		humanTestBf[0][2] = 2;
		humanTestBf[0][3] = 2;
		humanTestBf[0][4] = 2;
		
		humanBf = human.getBf().getField();
		assertEquals(humanTestBf, humanBf);
		assertEquals(1, human.getNumOfDest());
		
		gui.MainWindow mwin = new gui.MainWindow();
		gui.GameWindow gwin = mwin.getGameWin();
		gwin.setHumanPlayer(human);
		gwin.saveGame();
		
		gui.MainWindow mwin2 = new gui.MainWindow();
		gui.GameWindow gwin2 = mwin2.getGameWin();
		gwin2.loadGame(true);
		
		assertEquals(humanTestBf, gwin2.getHumanPlayer().getBf().getField());
	}
	
	public static void printField(int[][] f){
		for(int i = 0; i < 10; i++){
			for(int j = 0; j < 10; j++){
				System.out.print("| " + f[i][j] + " ");
			}
			System.out.print("|");
			System.out.println();
		}
	}
}
